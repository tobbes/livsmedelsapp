//
//  DetailViewController.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-26.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backend.h"

@interface DetailViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *mainTitle;
@property (weak, nonatomic) IBOutlet UITextField *fat;

@property (weak, nonatomic) IBOutlet UITextField *protein;
@property (weak, nonatomic) IBOutlet UITextField *energy;
@property (weak, nonatomic) IBOutlet UITextField *salt;

@property (weak, nonatomic) IBOutlet UIProgressView *nyttighet;

@property (nonatomic) int id;
@property (nonatomic) Backend * backend;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
