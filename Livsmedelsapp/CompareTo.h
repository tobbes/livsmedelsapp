//
//  CompareTo.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-03-17.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backend.h"
#import <GKBarGraph.h>
@interface CompareTo : UIViewController <GKBarGraphDataSource>
@property (nonatomic) int first;
@property (nonatomic) int second;
@property (nonatomic) Backend * backend;
@property (nonatomic) NSDictionary * food1;
@property (nonatomic) NSDictionary  * food2;
@end
