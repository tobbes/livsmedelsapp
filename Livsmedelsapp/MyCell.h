//
//  MyCell.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-03-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *energylabel;

@property (weak, nonatomic) IBOutlet UILabel *namelabel;

@end
