//
//  Backend.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-23.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^F)();

@interface Backend : NSObject

@property (nonatomic) NSArray *  foods;
-(void) LoadFoods: (UIViewController *) source;
- (void) getFood: (int) id withSet:(F) myblock;

-(void) addFavourite:(NSNumber *) favourite;
@property (nonatomic) NSMutableArray * favourites;

-(void) persistFavourites;

-(void) loadFavourites;

-(NSNumber *) calculateNyttighetgivenfat: (NSNumber *) fat protein: (NSNumber *) protein salt: (NSNumber *) salt;


@end
