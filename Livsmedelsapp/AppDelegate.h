//
//  AppDelegate.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-16.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

