//
//  CompareTo.m
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-03-17.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "CompareTo.h"

@interface CompareTo ()
@property (weak, nonatomic) IBOutlet UILabel *livs1;
@property (weak, nonatomic) IBOutlet UILabel *livs2;

@property (weak, nonatomic) IBOutlet GKBarGraph *bargraph;

@end


@implementation CompareTo




- (IBAction)onCancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}


-(void) viewDidLoad {
    
    
    
    //[self.backend get
    
    
    self.bargraph.barWidth = 10.0;
    self.bargraph.barHeight = self.bargraph.bounds.size.height * 0.9;
    self.bargraph.barWidth = self.bargraph.bounds.size.width/6;
    self.bargraph.dataSource = self;
    
    [self.backend getFood:self.first withSet: ^(NSData * data, NSURLResponse * response, NSError * error)
     {
         
         NSDictionary * result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
         
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
         
             self.food1 = result;
             self.livs1.text = result[@"name"];
             
             
             
             
             if(self.food2) {
             [self.bargraph draw];
             [self.view reloadInputViews];
                 //NSLog(@"%@",self.food2);
             NSLog(@"jepp jepp jepp 1");
             }
             
             
             });
         
     }];
    

    [self.backend getFood:self.second withSet:^(NSData * data, NSURLResponse *response, NSError * error) {
        
        NSDictionary * result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
      
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.food2 = result;
            
            self.livs2.text = result[@"name"];
            if(self.food1) {
            [self.bargraph draw];
            [self.view reloadInputViews];
               // NSLog(@"%@", self.food1);
            }
        });
        
        
    }];
    
}




- (NSInteger)numberOfBars {
    
    return 6;
    
    
}
- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    
    
    NSNumber * num;
    
    switch(index) {
        case 0: num = self.food1[@"nutrientValues"][@"fat"]; break;
        case 1: num = self.food2[@"nutrientValues"][@"fat"]; break;
        case 2: num = self.food1[@"nutrientValues"][@"protein"]; break;
        case 3: num = self.food2[@"nutrientValues"][@"protein"]; break;
        case 4: num =  self.food1[@"nutrientValues"][@"salt"]; break;
        case 5: num = self.food2[@"nutrientValues"][@"salt"]; break;
            
    }
    NSLog(@"värde: %@",num);
    return num;
}


- (UIColor *)colorForBarAtIndex:(NSInteger)index {
    
    if(index % 2 == 0){
        
        return [UIColor redColor];
    }
    else {
        return [UIColor blueColor];
    }
    
}



- (UIColor *)colorForBarBackgroundAtIndex:(NSInteger)index {
    
    return [UIColor whiteColor];
}




- (CFTimeInterval)animationDurationForBarAtIndex:(NSInteger)index {
    
    return 0.5;
    
}
- (NSString *)titleForBarAtIndex:(NSInteger)index {
    
    NSArray * titles = @[@"Fat",@"Fat",@"Protein",@"Protein",@"Salt",@"Salt"];

    
    //switch(index) {
            
           /* switch(index) {
                case 0: title = @"Fat"; break;
                case  1: title = @"Fat"; break;
                case 2: title = @"Protein"; break;
                case 3: title = @"Protein"; break;
                case 4: title = @"Salt"; break;
                case 5: title = @"Salt"; break;
    }*/

    
                
                return titles[index];
    
            
            
}








@end
