//
//  MenuController.m
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-23.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "MenuController.h"
#import "SearchTable.h"
#import "Favourites.h"

@interface MenuController ()
@property UIDynamicAnimator * animator;
@property UIAttachmentBehavior * attachment;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *favouriteButton;





@end

@implementation MenuController




-(void) viewDidLoad {
    
     self.animator =   [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
}



-(void) viewDidLayoutSubviews {
    
    //save original position of the searchbutton
    CGPoint orgpos = self.searchButton.center;
    
    //hide it
    self.searchButton.center =  CGPointMake(0 - self.searchButton.bounds.size.width/2,self.searchButton.center.y);
    //setup for the searchbutton to SNAP back
    UISnapBehavior * snap = [[UISnapBehavior alloc] initWithItem:self.searchButton snapToPoint:orgpos];
    [snap setDamping:0];
    [self.animator addBehavior:snap];
       
    //snap = [[UISnapBehavior alloc]initWithItem:<#(id<UIDynamicItem>)#> snapToPoint:<#(CGPoint)#>
    
    
    
}


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if([segue.identifier isEqualToString:@"doSearch"] ) {
    SearchTable * table = (SearchTable *) segue.destinationViewController;
    
        @try{
           table.foods = [self.backend.foods mutableCopy];
            table.backend = self.backend;
    }
        
        
        

        
        
    @catch(NSException * e) {
        NSLog(@"%@",e);
    }

}

    else {
        
    Favourites * favdest =    (Favourites *) segue.destinationViewController;
    
        favdest.backend = self.backend;
        
    }

}

@end
