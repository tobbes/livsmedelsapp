//
//  Favourites.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-03-12.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backend.h"
@interface Favourites : UITableViewController
@property (nonatomic)  Backend * backend;


@end
