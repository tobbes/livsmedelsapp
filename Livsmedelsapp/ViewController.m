//
//  ViewController.m
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-16.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "ViewController.h"
#import "Backend.h"
#import "MenuController.h"
@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    
    // Do any additional setup after loading the view, typically from a nib.
}



-(void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    
    if(!self.backend.foods) {
    @try {
        [self.backend LoadFoods:self];
    }
    @catch(NSException * e) {
        
        NSLog(@" Fel: %@",e);
    }
    }
}









- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
     MenuController * dest  = (MenuController *) segue.destinationViewController;
    dest.backend = self.backend;
    
    
}



@end
