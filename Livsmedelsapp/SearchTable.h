//
//  SearchTable.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-23.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "Backend.h"
#import <UIKit/UIKit.h>
#import "MyCell.h"

typedef void (^VoidFunc)();

@interface SearchTable : UITableViewController <UISearchBarDelegate>
@property Backend * backend;
@property (nonatomic) NSMutableArray * foods;
@property (nonatomic) int compare;

//@property (nonatomic) VoidFunc action;

@end
