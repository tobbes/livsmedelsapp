//
//  SearchTable.m
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-23.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "SearchTable.h"
#import "DetailViewController.h"
#import "CompareTo.h"
@interface SearchTable()
@property (strong, nonatomic) IBOutlet UITableView *searchfield;

@property (nonatomic) NSArray * searchResult;


@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swiperLeft;
@property (strong, nonatomic) NSMutableArray * searchResultExtraInfo;
@property (strong,nonatomic) NSMutableArray * ExtraInfo;

@end



@implementation SearchTable

@synthesize ExtraInfo = _ExtraInfo;
@synthesize searchResultExtraInfo = _searchResultExtraInfo;



-(NSMutableArray *) ExtraInfo{
    
    if(!_ExtraInfo) {
        _ExtraInfo = [[NSMutableArray alloc] initWithCapacity:self.backend.foods.count];
        for (int i = 0 ; i < self.backend.foods.count ; i++) {
        
            [_ExtraInfo addObject:[NSNull null]];
        }
        
    
    }
    return _ExtraInfo;
}




-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    
    UITableViewCell * cell = (UITableViewCell *) sender;
    
    
    
    //find the tableview of the calling cell
    UITableView * p  = (UITableView *) cell.superview.superview;
    
    //given this tableview get rowindex for the cell in this
    // tableview
    int index =  (int) [p indexPathForCell:cell].row;
    
    NSDictionary * foodelement;
    
    //
    if(p == self.tableView) {
    
        foodelement = self.backend.foods[index];
    }
    
    else {
        
        foodelement = self.searchResult[index];
    }
    
    
    
    
    if([segue.identifier isEqualToString:@"detail"]) {
        DetailViewController * c = (DetailViewController *) segue.destinationViewController;
    
        
        c.id = (int) [foodelement[@"number"] integerValue];
        c.backend = self.backend;
    }
    
    
    
    else {
        
        
        CompareTo * c = (CompareTo *) segue.destinationViewController;
        
        c.backend = self.backend;
        c.first = self.compare;
        c.second = (int) [foodelement[@"number"] integerValue];
        
        }

    }
    



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell;
    if(tableView == self.tableView) {
    
        cell = [self.tableView cellForRowAtIndexPath:indexPath];
    }
    
    else {
        
        cell = [tableView cellForRowAtIndexPath:indexPath];
    }
    
    
    
        if(!self.compare)
            [self performSegueWithIdentifier:@"detail" sender:cell];
    
        else {
            
            
            @try {
            [self performSegueWithIdentifier:@"compare" sender:cell];
            }
        
            @catch(NSException * e) {
                
                NSLog(@"fel vid perform segue: %@",e);
            }
            
        }
    
    
    
    
}
    





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //if the tableview is not this controllers tableview, then it must
    // be the searchcontrollers table view so return the searchresult's count
    
    if(tableView != self.tableView)
        return self.searchResult.count;
        
        
        else {
            return self.backend.foods.count;
        }

}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSLog(@"hello world!");
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@",searchText];
    
    self.searchResult = [self.backend.foods filteredArrayUsingPredicate:predicate];

}



-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

-(MyCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    
    NSArray * source;
    NSMutableArray * info;

    
    if(tableView != self.tableView && self.searchResult != nil) {
        source = self.searchResult;
        
        
    }
    
    else {
        source = self.backend.foods;
        info = self.ExtraInfo;
    }
    
    
    
    
    
    
    MyCell * cell;
    
    
    @try {
    
    
        
        
    static NSString *CellIdentifier = @"MyOtherCell";
    static NSString *NormalCell = @"normalCell";
    NSString * identifier;
        
        if(tableView == self.tableView) {
    
            identifier = CellIdentifier;
        }
        
        else {
            
            identifier = NormalCell;
        }
        
        
            cell = [self.tableView dequeueReusableCellWithIdentifier:identifier
                                    forIndexPath:indexPath];
    int index = (int)indexPath.row;
    
    NSDictionary * dict = source[index];
        
        
        // search being performed
        // so searchcontrollers tableview is in use
        // different contents in cell
        if(tableView != self.tableView) {
            cell.textLabel.text = dict[@"name"];
            
            return cell;
        }
        
        //if we are in the full table view, get the key for this food
        // to look up information about
        int id = [dict[@"number"] intValue];
        cell.namelabel.text = dict[@"name"];
        cell.namelabel.numberOfLines = 0;
        
        if(self.ExtraInfo[index] != [NSNull null]) {
            cell.energylabel.text = [NSString stringWithFormat:@"%@ Kcal",self.ExtraInfo[index]];
            return cell;
        }
        
        
        
        [self.backend getFood:id withSet:  ^(NSData * data, NSURLResponse * response, NSError * error) {
            
            NSError * parseError;
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            NSDictionary * nutrients = dict[@"nutrientValues"];
            
            self.ExtraInfo[index] = nutrients[@"energyKcal"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
            
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            //cell.energylabel.text = [NSString stringWithFormat:@"%@ Kcal", nutrients[@"energyKcal"]];
            //[cell reloadInputViews];
            });
            
        }];
        
    }
    
    @catch(NSException * e) {
        
        NSLog(@"%@",e);
    }
    
    //return cell;
    return cell;
}


-(void)viewDidLoad {
    
    self.searchfield.delegate = (id) self;
    
}



@end
