//
//  MenuController.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-23.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backend.h"
@interface MenuController : UIViewController
@property (nonatomic) Backend * backend;



@end
