    //
//  Favourites.m
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-03-12.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "Favourites.h"
#import "DetailViewController.h"
@interface Favourites ()




@end



@implementation Favourites



- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.backend.favourites.count;
    
}



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    UITableViewCell * cell = (UITableViewCell *) sender;
    int index = (int) [self.tableView indexPathForCell:cell].row;
    
    
    NSDictionary * info = self.backend.favourites[index];
    
    
    DetailViewController * dest = (DetailViewController *) segue.destinationViewController;
    
    dest.backend = self.backend;
    
    NSNumber * number = info[@"id"];
    
    dest.id = number.intValue;
    
    
    
    
    
}





-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * identifier = @"favouriteCell";
    
    UITableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    
    int index  =  (int) indexPath.row;
    NSDictionary * entry = self.backend.favourites[index];
    
    cell.textLabel.text = [entry objectForKey:@"name"];
    
    
    return cell;
}





@end
