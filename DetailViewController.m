//
//  DetailViewController.m
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-02-26.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "DetailViewController.h"
#import "SearchTable.h"
@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UIButton *favoritButton;



@end

@implementation DetailViewController

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    UIImage * img = info[UIImagePickerControllerOriginalImage];
    [self saveImage:img];
    
    self.image.image = img;
    [picker dismissViewControllerAnimated:YES completion:nil];
    

    
    
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
 
}

- (IBAction)addFavourite:(id)sender {
    
    NSDictionary * favorit = [[NSDictionary alloc] init];
  
    
    
    
    favorit = @{@"id" : [NSNumber numberWithInt:self.id], @"name" : self.mainTitle.text};
    
    
    if(![self.backend.favourites containsObject:favorit]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Added to favorites" message:[NSString stringWithFormat:@"Added %@",favorit[@"name"]]
                               delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
        alert.message = [NSString stringWithFormat:@"Lagt till: %@ som favorit",favorit[@"name"]];
        alert.delegate = self;
        [alert show];
        [self.backend.favourites addObject:favorit];
        [self.favoritButton setTitle:@"Radera favrorit" forState:UIControlStateNormal];
        [self.favoritButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        
        
    }
    
    
    else {
       
    
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Raderat favorit!"
                                                         message:[NSString stringWithFormat:@" Raderat favroiten %@ .",favorit[@"name"]] delegate:self cancelButtonTitle:@"OK"otherButtonTitles:nil];
        [alert show];
        [self.backend.favourites removeObject:favorit];
        [self.favoritButton setTitle:@"Favorit" forState:UIControlStateNormal];
        [self.favoritButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    
    }
    
    
    
    
    
    NSLog(@"Favourites: %@",self.backend.favourites);
    
    
}


-(NSString *) getPath  {
    
    
    
    NSString * imgname = [NSString stringWithFormat:@"%d.png",self.id];
    NSArray *paths =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:
                           imgname];
    
    return imagePath;
}



-(void) saveImage: (UIImage *) image {
    
    
    
    
    NSString * path = [self getPath];
    NSData * imagedata = UIImagePNGRepresentation(image);
    
    if(![imagedata writeToFile:path atomically:YES])
    {
        NSLog(@"could not save image because of reasons");
    }
}


-(void) viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    
    UIImage * img =[self loadImage];
    
    if(img) {
        self.image.image = img;
    
        CGPoint orgpos = self.image.center;
        
        self.image.center = CGPointMake((0-img.size.width), orgpos.y);
        
        [UIView animateWithDuration:2.0f delay:0.0 options:kNilOptions animations:^ {
            self.image.center = orgpos;
            
            
        }completion:nil];
        
    
    }
    
    
}


-(UIImage *) loadImage {
    
    NSString * path = [self getPath];
    
    UIImage * image = [UIImage imageWithContentsOfFile:path];
    return image;
    
}

- (IBAction)onTakePicture:(id)sender {
    
    
    UIImagePickerController * controller = [[UIImagePickerController alloc] init];
    
    [controller setAllowsEditing:NO];
    controller.delegate = self;
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)onCompare:(id)sender {
    
    SearchTable *searchTable = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableView"];
    
    searchTable.backend = self.backend;
    searchTable.compare = self.id;
    
//    [self presentViewController:searchTable animated:YES completion:nil];
    
    [self.navigationController pushViewController:searchTable animated:YES];
    
    
    
}

    
    
    
    
-(void) viewDidLoad {
    
    NSLog(@"%d",self.id);
    


     void (^simpleBlock)(NSData *, NSURLResponse *, NSError *) =
     
     
     ^(NSData * data, NSURLResponse * response, NSError * error) {
     
     NSError * parseError;
     NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
     dispatch_async(dispatch_get_main_queue(), ^{
     
         if([self.backend.favourites containsObject:@{@"id" : dict[@"number"],@"name" : dict[@"name"]} ]){
             
             
             
             
             [self.favoritButton setTitle:@"Radera favorit" forState:UIControlStateNormal];
             
             [self.favoritButton setTitleColor: [UIColor redColor] forState:UIControlStateNormal];
             
         }
         
         
         NSDictionary * nutrients = dict[@"nutrientValues"];
         self.mainTitle.text = dict[@"name"];
         self.fat.text = [NSString stringWithFormat:@"%@ g",nutrients[@"fat"]];
         self.protein.text = [NSString stringWithFormat:@"%@ g",nutrients[@"protein"]];
         self.salt.text = [NSString stringWithFormat:@"%@ g",nutrients[@"salt"]];
         self.energy.text = [NSString stringWithFormat:@"%@",nutrients[@"energyKcal"]];
         NSNumber * nyttighet = [self.backend calculateNyttighetgivenfat:nutrients[@"fat"] protein: nutrients[@"protein"] salt:nutrients[@"salt"]];
        
         //self.nyttighet.progress = nyttighet.floatValue;
         self.nyttighet.progress = nyttighet.floatValue;
         
     
     
     
     });
     
     
     };
    
    [self.backend getFood:self.id withSet:simpleBlock];
   

}
@end
