//
//  CustomCell.h
//  Livsmedelsapp
//
//  Created by Tobias Ednersson on 2015-03-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@end
